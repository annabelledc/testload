
import createUser from "./test/createuser.js";
import deleteUser from "./test/deletuser.js";
import getAllUsers from "./test/getallusers.js";
import getUser from "./test/getuser.js";
import updateUser from "./test/updateuser.js";
import http from "k6/http";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js'


export function handleSummary(data) {
  return {
    './reports/results.html': htmlReport(data),
    stdout: textSummary(data, { indent: ' ', enableColors: true }),
  };
}


export let options = {
    stages: [ // ramp up. ramp up to 50 users in one minute, run the 50 concurrent users for 2 minutes, then ramp down to 0 VU in 1 minute
      { duration: '1s', target: 5 },

    ],
    thresholds: { //rate is error rate. if it exceeds 0.01, the test stops and is reported as failed.
        http_req_failed: [{threshold:'rate<0.30', abortOnFail: true, delayAbortEval: '10s'},],   // http errors should be less than 1%
        http_req_duration: [{threshold:'p(90)<500', abortOnFail: true, delayAbortEval: '10s'},], // 90% of requests should be below 500ms
        http_reqs: [{threshold:'rate<500', abortOnFail: true, delayAbortEval: '10s'},], // http_reqs rate should be below 500ms. this is the average response time

      },


  };

export default function setUp(){
    createUser();

}
