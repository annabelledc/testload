import http from "k6/http";
import { base_url,access_token } from "../config.js";
import { randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import { check } from "k6";

export var user_id;
export var headers;

 export default function createUser(){
     headers = {
        "Content-type": "application/json",
        "Authorization": `Bearer ${access_token}`
    };
    var rand = randomIntBetween(0, 9999999);
    let data = {
        
            "name": "Rep. Subhasini Somayaji",
            "email": `teats${rand}@test.com`,
            "gender": "male",
            "status": "inactive"
          
    }

    let response = http.post(base_url + "users", JSON.stringify(data), {
        headers: headers,
      }); 
      check(response, {
        'User created successfully': (res) => res.status === 201,
        "Create user response body contains id ": (res) => (typeof res.json()["id"] !== "undefined"),
      });
      user_id = response.json()["id"];
      // console.log("ID IS:  " + response.json()["id"]); 
      // console.log("NAME IS:  " + response.json()["name"]); 
      // console.log("email IS:  " + response.json()["email"]); 

      


}